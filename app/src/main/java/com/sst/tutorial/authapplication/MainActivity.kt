package com.sst.tutorial.authapplication

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.facebook.*
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.*
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private var callbackManager: CallbackManager = CallbackManager.Factory.create()
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var authListener: FirebaseAuth.AuthStateListener
    private lateinit var facebookCallback: FacebookCallback<LoginResult>
    private lateinit var accessTokenTracker: AccessTokenTracker
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var googleCompleteListener: OnCompleteListener<AuthResult>



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        extractedFromOnCreate()
    }


    private fun extractedFromOnCreate() {
        initLateinitVars()

        facebookLoginButton?.apply {
            setPermissions(EMAIL, PROFILE)
            registerCallback(callbackManager, facebookCallback)
        }

        googleSignInButton.setOnClickListener { googleSignIn() }
        googleSignOutButton.setOnClickListener { googleSignOut() }
    }

    // Necessary overloads

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == RC_SIGN_IN) {
            handleGoogleSignInResult(
                GoogleSignIn.getSignedInAccountFromIntent(data)
            )
        }
    }

    override fun onStart() {
        super.onStart()
        firebaseAuth.addAuthStateListener(authListener)


    }

    override fun onStop() {
        super.onStop()
        firebaseAuth.removeAuthStateListener(authListener)
    }


    // Utilitarian methods

    private fun initLateinitVars() {
        firebaseAuth = FirebaseAuth.getInstance()

        // Facebook related
        facebookCallback = object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                Log.d(TAG, "FbCallback.onSuccess")
                result?.apply { handleFacebookToken(accessToken) }
            }

            override fun onCancel() {
                Log.d(TAG, "FbCallback.onCancel")
            }

            override fun onError(error: FacebookException?) {
                Log.d(TAG, "FbCallback.onError: ${error?.message}")
            }
        }

        authListener = FirebaseAuth.AuthStateListener { updateUI(it.currentUser) }
        accessTokenTracker = object : AccessTokenTracker() {
            override fun onCurrentAccessTokenChanged(
                oldAccessToken: AccessToken?,
                currentAccessToken: AccessToken?
            ) {
                if (null == currentAccessToken) {
                    firebaseAuth.signOut()
                }
            }
        }

        // Google related
        val googleSignInOpt = GoogleSignInOptions
            .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOpt)

        googleCompleteListener = OnCompleteListener{ task ->
                if(task.isSuccessful) {
                    Log.d(TAG, "Login task is SUCCESSFUL")
                    firebaseAuth.currentUser
                } else {
                    Log.d(TAG, "Login task FAILED")
                    null
                }.let {
                    updateUI(it)
                }
            }
    }

    private fun googleSignIn() {
        startActivityForResult(
            googleSignInClient.signInIntent,
            RC_SIGN_IN
        )
    }

    private fun googleSignOut() {
        googleSignInClient.signOut()
        firebaseAuth.signOut()
    }

    private fun handleFacebookToken(token: AccessToken) {
        val credentials = FacebookAuthProvider.getCredential(token.token)
        firebaseAuth.signInWithCredential(credentials)
            .addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    Log.d(TAG, "signInWithCredential: SUCCESS")

                    googleSignOutButton.visibility = View.VISIBLE
                    googleSignInButton.visibility = View.GONE

                    firebaseAuth.currentUser
                } else {
                    Log.d(TAG, "signInWithCredential: FAILED ${task.exception}")
                    Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
                    null
                }.let { user ->
                    updateUI(user)
                }

            }
    }

    private fun handleGoogleSignInResult(task: Task<GoogleSignInAccount>) {
        try {
            val account = task.getResult(ApiException::class.java)
            Toast.makeText(this, "Google sign in SUCCESSFUL", Toast.LENGTH_SHORT).show()

            account

        } catch (e: ApiException) {
            Toast.makeText(this, "Google sign in FAILED", Toast.LENGTH_LONG).show()
            null
        }.let {
            firebaseGoogleAuth(it)
        }
    }

    private fun firebaseGoogleAuth(user: GoogleSignInAccount?) {
        user?.let {
            val authCredential = GoogleAuthProvider.getCredential(it.idToken, null)
            firebaseAuth
                .signInWithCredential(authCredential)
                .addOnCompleteListener(this, googleCompleteListener)
        }
            ?: Toast.makeText(this, "Google account failed", Toast.LENGTH_LONG).show()
    }

    private fun updateUI(user: FirebaseUser?) {
        if (null != user) {
            Glide.with(this)
                .load(user.photoUrl)
                .into(profileImage)

            textField.text = user.displayName
            connectionStatus.text = getString(R.string.connected_facebook)
        } else {
            Glide.with(this)
                .load(R.drawable.firebase_logo)
                .into(profileImage)

            textField.text = ""
            connectionStatus.text = getString(R.string.not_connected)

            googleSignInButton.visibility = View.VISIBLE
            facebookLoginButton.visibility = View.VISIBLE
        }
    }

    companion object {
        const val EMAIL = "email"
        const val PROFILE = "public_profile"
        const val TAG = "MainActivity"
        const val RC_SIGN_IN = 0x1111 // idk
    }
}
